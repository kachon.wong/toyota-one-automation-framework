package autoframework.api.controller;

import autoframework.api.base.HttpClient;
import autoframework.api.base.HttpClientResult;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class AzureController {
    private static Logger logger = LogManager.getLogger(AzureController.class);

    public static String getToken(HttpClient httpClient) throws Exception {
        String path = "/f855c7e1-6646-4300-8de1-10c7d3001287/oauth2/token";
        Map<String, String> headers = new HashedMap();
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        Map<String, String> params = new HashedMap();
        params.put("client_id", "0bdec407-c269-499c-b39a-1e071e9b3ab8");
        params.put("client_secret", "VwZQ1c9+GSXkYoipZgnFjzcpxyDWJvxrYAmUMhPOPiw=");
        params.put("grant_type", "client_credentials");

        HttpClientResult httpClientResult = httpClient.doPost(path, headers, params);
        JsonObject returnData = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject();
        String token = returnData.get("access_token").getAsString();
        logger.info("The token for Azure is: "+ token);
        return token;
    }

    public static boolean checkUserExist(HttpClient httpClient, String token, String emailOrPhoneNumber) throws Exception {
        String path = "/users";

        Map<String, String> headers = new HashedMap();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Bearer " + token);

        Map<String, String> params = new HashedMap();
        params.put("api-version", "1.6");
        params.put("$filter", String.format("signInNames/any(x:x/value eq '%s')", emailOrPhoneNumber));

        HttpClientResult httpClientResult = httpClient.doGet(path, headers, params);
        JsonArray jsonArray = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject().get("value").getAsJsonArray();
        return jsonArray.size() > 0;
    }

    public static boolean emailVerifiedFlagVerification(HttpClient httpClient, String email) throws Exception {
        String path = "/openidm/managed/user";

        Map<String, String> headers = new HashedMap();
        headers.put("X-OpenIDM-Username", "idmadmin");
        headers.put("X-OpenIDM-Password", "ttsju0lHfxwMxgkf9p");

        Map<String, String> params = new HashedMap();
        params.put("_queryFilter", String.format("mail eq \"%s\"", email));

        HttpClientResult httpClientResult = httpClient.doGet(path, headers, params);
        JsonArray jsonArray = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject().get("result").getAsJsonArray();

        if(jsonArray.size() == 0) {
            return false;
        } else {
            JsonElement element = jsonArray.get(0).getAsJsonObject().get("tmnaMailVerified");
            if (element == null) {
                return false;
            }
            return element.getAsBoolean();
        }
    }

    public static String getTokenForOCPRDataBaseValidation(HttpClient httpClient) throws Exception {
        String path = "/f855c7e1-6646-4300-8de1-10c7d3001287/oauth2/token";

        Map<String, String> headers = new HashedMap();
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        Map<String, String> params = new HashedMap();
        params.put("client_id", "f7165ade-8590-4a81-b019-8aea93769b0e");
        params.put("client_secret", ">9Spw)L1k]cGnU3Dr*e:sm7l");
        params.put("grant_type", "client_credentials");
        params.put("resource", "https://tmnab2cqa.onmicrosoft.com/CTPAPIDev");

        HttpClientResult httpClientResult = httpClient.doPost(path, headers, params);
        JsonObject returnData = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject();
        String token = returnData.get("access_token").getAsString();
        logger.info("The token for OCPRDataBaseValidation is: "+ token);
        return token;
    }

    public static boolean isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(HttpClient httpClient, String token, String email) throws Exception {
        String path = "/v1/account";

        Map<String, String> headers = new HashedMap();
        headers.put("content-type", "application/json");
        headers.put("x-brand", "T");
        headers.put("X-CHANNEL", "CT_FR");
        headers.put("x-correlationid", "456e4567-e89b-12d3-a456-426655440001");
        headers.put("x-api-key", "HCskiPdz933jkEoH4AQ6r1zh3Rp6Totw91wPy93q");
        headers.put("email", email);
        headers.put("Authorization", "Bearer " + token);

        HttpClientResult httpClientResult = httpClient.doGet(path, headers, null);
        JsonObject jsonObject = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject().get("status").getAsJsonObject();

        if (jsonObject.get("messages") != null && jsonObject.get("messages").getAsJsonArray().size() > 0  && jsonObject.get("messages").getAsJsonArray().get(0).getAsJsonObject().get("detailedDescription") != null && StringUtils.endsWithIgnoreCase(jsonObject.get("messages").getAsJsonArray().get(0).getAsJsonObject().get("detailedDescription").getAsString(), "User Not Found")) {
            return false;
        } else {
          try {
              jsonObject = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject();
              JsonArray jsonArray = jsonObject.get("payload").getAsJsonObject().get("customer").getAsJsonObject().get("emails").getAsJsonArray();
              for(int i = 0; i < jsonArray.size(); i++) {
                  if (StringUtils.endsWithIgnoreCase(jsonArray.get(i).getAsJsonObject().get("emailAddress").getAsString(), email)) {
                        return true;
                  }
              }
              return false;
          } catch (Exception err){
            logger.error(err);
            return false;
          }
        }
    }

    public static boolean isUserCreatedByUsingOCPRDataBaseValidationAlignWithPhone(HttpClient httpClient, String token, String phone) throws Exception {
        String path = "/v1/account";

        Map<String, String> headers = new HashedMap();
        headers.put("content-type", "application/json");
        headers.put("x-brand", "T");
        headers.put("X-CHANNEL", "CT_FR");
        headers.put("x-correlationid", "456e4567-e89b-12d3-a456-426655440001");
        headers.put("x-api-key", "HCskiPdz933jkEoH4AQ6r1zh3Rp6Totw91wPy93q");
        headers.put("phone", phone);
        headers.put("Authorization", "Bearer " + token);

        HttpClientResult httpClientResult = httpClient.doGet(path, headers, null);
        JsonObject jsonObject = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject().get("status").getAsJsonObject();

        if (jsonObject.get("messages") != null && jsonObject.get("messages").getAsJsonArray().size() > 0  && jsonObject.get("messages").getAsJsonArray().get(0).getAsJsonObject().get("detailedDescription") != null && StringUtils.endsWithIgnoreCase(jsonObject.get("messages").getAsJsonArray().get(0).getAsJsonObject().get("detailedDescription").getAsString(), "User Not Found")) {
            return false;
        } else {
            try {
                jsonObject = new JsonParser().parse(httpClientResult.getContent()).getAsJsonObject();
                JsonArray jsonArray = jsonObject.get("payload").getAsJsonObject().get("customer").getAsJsonObject().get("phoneNumbers").getAsJsonArray();
                for(int i = 0; i < jsonArray.size(); i++) {
                    if (StringUtils.endsWithIgnoreCase(jsonArray.get(i).getAsJsonObject().get("phoneNumber").getAsString(), phone)) {
                        return true;
                    }
                }
                return false;
            } catch (Exception err){
                logger.error(err);
                return false;
            }
        }
    }
}