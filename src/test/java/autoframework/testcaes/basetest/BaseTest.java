package autoframework.testcaes.basetest;

import autoframework.common.webutils.WebUtils;
import autoframework.listener.ScreenCaptureListener;
import autoframework.pojo.EmailUser;
import autoframework.pojo.EmailUsers;
import autoframework.testcaes.helper.*;
import autoframework.utils.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Listeners({ScreenCaptureListener.class})
public class BaseTest {

    public static AppiumDriver<MobileElement> driver;
    protected static Logger logger = LogManager.getLogger(AppiumDriver.class);
    public static EmailUser emailUser;
    public static EmailUser alreadyEmailuser;

    protected StartPageHelper startPageHelper;
    protected ContinueRegisterPageHelper continueRegisterPageHelper;
    protected RegisterPageHelper registerPageHelper;
    protected VerificationCodePageHelper verificationCodePageHelper;
    protected AddVehiclePageHelper addVehiclePageHelper;
    protected LoginForAccountPageHelper loginForAccountPageHelper;
    protected LoginForPasswordPageHelper loginForPasswordPageHelper;

    protected WebUtils webUtils;

    @BeforeSuite
    public void setUp() throws Exception {
        logger.info("********************测试套件开始执行********************");
        initAndroidDriver();
        initEmailUsers();
//        initIOSDriver();
//        GlobalVariable.init();
    }

    @BeforeClass
    public void classSetup() throws Exception {
        startPageHelper = new StartPageHelper(driver);
        continueRegisterPageHelper = new ContinueRegisterPageHelper(driver);
        registerPageHelper = new RegisterPageHelper(driver);
        verificationCodePageHelper = new VerificationCodePageHelper(driver);
        addVehiclePageHelper = new AddVehiclePageHelper(driver);
        loginForAccountPageHelper = new LoginForAccountPageHelper(driver);
        loginForPasswordPageHelper = new LoginForPasswordPageHelper(driver);
        webUtils =  new WebUtils(driver);
    }

    @BeforeMethod
    public void methodSetup() throws MalformedURLException {

    }

    @AfterMethod
    public void afterMethod() {
        logger.info("Close driver");
    }

    @AfterSuite
    public void closeDriver() {
        logger.info("********************Suit test end********************");
        driver.resetApp();
        driver.quit();
    }

    public static void initAndroidDriver() throws MalformedURLException {
        logger.info("********************init Android Driver*******************");
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", System.getProperty("APPIUM_DEVICE_NAME", "WCR0218320001804"));
        capabilities.setCapability("platformVersion", System.getProperty("APPIUM_DEVICE_VERSION", "10"));
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("platformName", System.getProperty("APPIUM_PLATFORM", "Android"));
        capabilities.setCapability("appPackage", "com.toyota.oneapp.stage");
        capabilities.setCapability("appActivity", "com.toyota.oneapp.ui.splash.SplashActivity");
        capabilities.setCapability("noReset", true);
        capabilities.setCapability("newCommandTimeout", 7200);
        //capabilities.setCapability("unicodeKeyboard", true);
        //capabilities.setCapability("resetKeyboard", true);

        capabilities.setCapability("chromedriverChromeMappingFile", Utils.getAbsolutePath("chromdrivermap/map.json"));
        capabilities.setCapability("chromedriverExecutableDir", Utils.getAbsolutePath("chromedriver"));

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability("browserName", "");
        //chromeOptions.setExperimentalOption("w3c", false);
        capabilities.merge(chromeOptions);

        /**
         * need to add x right to file
         */
        File file = new File(Utils.getAbsolutePath("chromedriver"));
        List<File> files = Arrays.asList(file.listFiles());

        files.forEach(ele -> {
            try {
                ele.getAbsolutePath();
                Runtime.getRuntime().exec(String.format("chmod +x %s", ele.getAbsolutePath()));
            } catch (IOException ignore) {
                //ignore.printStackTrace();
            }
        });

        try {
            driver = new AndroidDriver(new URL(System.getProperty("APPIUM_URL", "http://127.0.0.1:4723/wd/hub")), capabilities);
        } catch (Exception err) {
            logger.info("Android Driver init failed: ");
            logger.error(err);
            throw err;
        }

        logger.info("Android Driver init successfully");
    }

    public static void initEmailUsers() throws Exception {
        logger.info("******************** Obtain EmailUsers List to get on available email address *******************");
        EmailUsers emailUsers = Utils.deserialize("users/email_users.json", EmailUsers.class);
        List<EmailUser> emailUserList = emailUsers.getUsers();
        for (int i = 0; i <= emailUserList.size(); i++) {
            if (!APIHelper.checkUserExistInAzure(emailUserList.get(i).getEmail())) {
                emailUser = emailUserList.get(i);
                break;
            }else {
                continue;
            }
        }
    }

    public static void initAlreadyEmailUser(){
        logger.info("******************** already in using *******************");
        Utils.addAttachment("data", Utils.getAbsolutePath("users/email_user.json"));
        alreadyEmailuser = Utils.deserialize("users/email_user.json", EmailUser.class);
    }

    public static void initIOSDriver() throws MalformedURLException {
        logger.info("********************Start to test suit********************");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", System.getProperty("APP_DEVICE_NAME", "iPhone"));
        capabilities.setCapability("platformVersion", System.getProperty("APP_DEVICE_VERSION", "12.2"));
        capabilities.setCapability("automationName", "XCUITest");
        capabilities.setCapability("udid", System.getProperty("APPIUM_DEVICE_UDID", "029d553ea04ba899509dc0630fda19bdac61231a"));
        capabilities.setCapability("platformName", System.getProperty("APP_DEVICE_PLATFORMNAME", "iOS"));
        capabilities.setCapability("bundleId", System.getProperty("APP_BUNDLEIDENTIFIER", "com.vwfsag.fsco.dealer"));
        capabilities.setCapability("webDriverAgentUrl", System.getProperty("WEBDRIVERAGENT_URL", "http://localhost:8100"));
        capabilities.setCapability("newCommandTimeout", 7200);
        capabilities.setCapability("startIWDP", true);
        capabilities.setCapability("unicodeKeyboard", "true");
        capabilities.setCapability("resetKeyboard", "true");

        try {
            driver = new IOSDriver<>(new URL(System.getProperty("APPIUM_URL", "http://127.0.0.1:4723/wd/hub")), capabilities);
        } catch (Exception err) {
            logger.error(err);
            throw err;
        }
        logger.info("init driver");
    }

    public AppiumDriver<MobileElement> getDriver() {
        return this.driver;
    }
}
