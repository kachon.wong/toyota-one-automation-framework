package autoframework.testcaes.security;

import autoframework.utils.Utils;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

@Epic("Epic: Local Account Sign Up- with activation")
@Feature("Feature: Security Test")
public class SecurityTest {

    //@Test(description = "securityTest01")
    public void securityTest01() {
        Utils.sleepBySecond(1);
    }

    //@Test(description = "securityTest02")
    public void securityTest02() {
        Utils.sleepBySecond(2);
    }

    //@Test(description = "securityTest03")
    public void securityTest03() {
        Utils.sleepBySecond(3);
    }
}
