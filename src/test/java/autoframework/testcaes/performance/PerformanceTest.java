package autoframework.testcaes.performance;

import autoframework.testcaes.helper.APIHelper;
import io.qameta.allure.*;
import org.testng.annotations.Test;

import static io.qameta.allure.Allure.step;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Epic("Epic: Local Account Sign Up- with activation")
@Feature("Feature: Performance Test")
public class PerformanceTest {

    @Story("7. IDM calls AZ API to check the user account")
    //@Test(description = "The mailbox provided by the user is available for registration", timeOut = 10000)
    @Description("Case Steps: " +
            "1. Call API to check the email：tao_toyota_test_not_used@126.com [7] \n" +
            "Expected Result: the email is available for registration")
    public void performanceTest01() throws Exception {
        String userEmail = "tao_toyota_test_not_used@126.com";
        step("The email provided by the user  is: " + userEmail);
        emailIsAvailable(userEmail);
    }

    @Step("The email is available for registration - [7]")
    public void emailIsAvailable(String userEmail) throws Exception {
        boolean result = APIHelper.checkUserExistInAzure(userEmail);
        assertThat("The email not is available for registration", result, is(false));
    }


    @Story("7. IDM calls AZ API to check the user account")
    //@Test(description = "The mailbox provided by the user is not available for registration", timeOut = 10000)
    @Description("Case Steps: " +
            "1. Call API to check the email：curefun001@126.com " +
            "Expected Result: the email is not available for registration")
    public void performanceTest02() throws Exception {
        String userEmail = "curefun001@126.com";
        step("The email provided by the user  is: " + userEmail);
        emailIsNotAvailable(userEmail);
    }

    @Step("The email is not available for registration - [7]")
    public void emailIsNotAvailable(String userEmail) throws Exception {
        boolean result = APIHelper.checkUserExistInAzure(userEmail);
        assertThat("The email not is available for registration", result, is(true));
    }

    @Story("14. EmailVerified Flag-Verfication")
    //@Test(description = "The mailbox provided by the user is not activated, the EmailVerified flag is false", timeOut = 10000)
    @Description("Case Steps: " +
            "1. Call API to check the email：curefun001@126.com " +
            "Expected Result: the EmailVerified flag is false")
    public void performanceTest03() throws Exception {
        String userEmail = "curefun001@126.com";
        step("The email provided by the user  is: " + userEmail);
        emailVerifiedFlagIsFalse(userEmail);
    }

    @Step("The email verified flag is false - [7]")
    public void emailVerifiedFlagIsFalse(String userEmail) throws Exception {
        boolean result = APIHelper.emailVerifiedFlagVerification(userEmail);
        assertThat("The email verified flag is true", result, is(false));
    }

    @Story("14. EmailVerified Flag-Verfication")
    //@Test(description = "The mailbox provided by the user is activated, the EmailVerified flag is true", timeOut = 10000)
    @Description("Case Steps: " +
            "1. Call API to check the email：tao_toyota_test3@126.com" +
            "Expected Result: the EmailVerified flag is true")
    public void performanceTest04() throws Exception {
        String userEmail = "tao_toyota_test3@126.com";
        step("The email provided by the user  is: " + userEmail);
        emailVerifiedFlagIsTrue(userEmail);
    }

    @Step("The email verified flag is true - [7]")
    public void emailVerifiedFlagIsTrue(String userEmail) throws Exception {
        boolean result = APIHelper.emailVerifiedFlagVerification(userEmail);
        assertThat("The email verified flag is false", result, is(true));
    }
}
