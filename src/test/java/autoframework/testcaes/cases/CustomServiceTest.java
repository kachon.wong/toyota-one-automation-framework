package autoframework.testcaes.cases;

import autoframework.utils.Utils;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

@Ignore
@Epic("Epic: Local Account Sign Up- with activation")
@Feature("Feature: CustomService Test")
public class CustomServiceTest {

    //@Test(description = "customServiceTest01")
    public void customServiceTest01() {
        Utils.sleepBySecond(1);
    }

    //@Test(description = "customServiceTest02")
    public void customServiceTest02() {
        Utils.sleepBySecond(2);
    }

    //@Test(description = "customServiceTest03")
    public void customServiceTest03() {
        Utils.sleepBySecond(3);
    }
}
