package autoframework.testcaes.cases;

import autoframework.common.webutils.WebUtils;
import autoframework.mail.ReceiveMail;
import autoframework.pojo.UsedUser;
import autoframework.testcaes.basetest.BaseTest;
import autoframework.testcaes.helper.APIHelper;
import autoframework.utils.Utils;
import io.appium.java_client.MobileElement;
import io.qameta.allure.*;
import org.apache.xml.utils.SystemIDResolver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.sql.SQLOutput;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;

@Epic("Epic: Local Account Sign Up- with activation")
@Feature("Feature: Function Test")
public class RegisterTest extends BaseTest {

    @Story("Story: Test register")
    @Test(description = "Case-1: User can register with valid and not registered email",priority  = 5)
    @Description("Case Steps: " +
            "1. User clicks Register on the OneApp - [1][2][3]\n " +
            "2. User is routed to the registration screen on OneApp\n " +
            "3. User clicks Continue Continue To Registration on the OneApp\n " +
            "4. User is routed to the registration screen on OneApp - [4]\n " +
            "5. User enters FN,LN,email,PhoneNO and Password(mandate fields) - [5] [6]  [9] \n " +
            "6. Use the API check if the email is already in use - [7] [8] [8.1][8.2] [8.2.1] [8.2.2]\n " +
            "7. User get activation code on the email, then input it- [10] [11] [12] [13] [14] [16]\n " +
            "8. User clicks Verify Account button on the OneApp - [17] [18] [19] [20] [21] " +
            "Expected Result: Then add VIN code screen displayed")
    public void testCaseForEmail_01() throws Exception {
        driver.resetApp();
        String path = Utils.getAbsolutePath("email/126.properties");
        //Utils.addAttachment("data", Utils.getAbsolutePath("users/email_users.json"));
        startPageHelper.goRegister();
        continueRegisterPageHelper.goRegister();
        Date date = new Date();
        if (emailUser == null){
            logger.info("All email is already in use");
            assertThat("All email is already in use",false);
        }else {
            Allure.addAttachment("data", emailUser.toString());
            registerPageHelper.checkUserIsNotAlreadyInAzure(emailUser.getEmail());

            logger.info("email is not used : " + emailUser.getEmail());
            registerPageHelper.inputRegisterInfo(emailUser.getFirstName(), emailUser.getLastName(), emailUser.getEmail(),
                    emailUser.getPhone(), emailUser.getPasswordForEmail());

            verificationCodePageHelper.checkVerificationCodePage();
            verificationCodePageHelper.checkUserNotActivate(emailUser.getEmail());

            ReceiveMail receiveMail = new ReceiveMail(emailUser.getEmail(), emailUser.getPasswordForAccount(), path);
            String code = receiveMail.verificationCode("Your Toyota App Verification Code", date, "(?<=verification code: )\\d+");
            logger.info("get verification code : " + code);
            verificationCodePageHelper.inputVerificationCode(code);
            addVehiclePageHelper.checkPageLoaded();
            verificationCodePageHelper.checkUserActivate(emailUser.getEmail());
            addVehiclePageHelper.checkUserRegisterSuccessInOCPRDataBase(emailUser.getEmail());
        }
    }

    @Story("Story: Test login in")
    @Test(description = "Case-2: User can login with registered account",priority = 6)
    @Description("Case Steps: " +
            "1. User clicks on Sign In  on the OneApp - [1][2][3]\n " +
            "2. User is routed to the Welcome back screen on OneApp\n " +
            "3. User enter Email\n " +
            "4. Use the API check if the email is already in use - [7] [8] [8.1] [8.1.1] [8.1.3] [8.1.4]\n" +
            "5. User clicks Continue button on the OneApp\n " +
            "6. User is routed to the Enter your password  screen on OneApp\n " +
            "7. user enter password\n " +
            "8. User click Sigin In button on the OneApp - [22]\n " +
            "Expected Result: User is routed to the enter VIN code screen on OneApp ")
    public void testCaseForEmail_02() throws Exception {
        driver.resetApp();
        startPageHelper.goLogin();
        String email = "test_Toyota001@126.com";
        String password = "TestBbc100004";
        assertThat("The email has been activated ",APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(email));
        if(APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(email)){
            loginForAccountPageHelper.inputAccount(email);
            loginForPasswordPageHelper.inputPassword(password);
            addVehiclePageHelper.checkPageLoaded();
        }else {
            assertThat("This email is already in register",false);
        }
    }


    @Story("test Register with email")
    @Test(description = "Case-3: User cannot register with registered email",priority = 1)
    @Description("Case Steps: " +
            "1. User clicks on Register on the OneApp- [1][2][3]\n " +
            "2. User is routed to the registration screen on OneApp\n " +
            "3. User clicks on Register Continue To Registration on the OneApp\n " +
            "4. User is routed to the registration screen on OneApp- [4]\n " +
            "5. User enters registered email - [6][7][8][8.1][8.1.1][8.1.3][8.1.4] \n" +
            "Expected Result: System prompt: This email is already in use. Please try siging in ")
    public void testCaseForEmail_03() throws Exception {
        driver.resetApp();
        startPageHelper.goRegister();
        continueRegisterPageHelper.goRegister();
        Utils.addAttachment("data", Utils.getAbsolutePath("users/useduser.json"));
        UsedUser usedUser = Utils.deserialize("users/useduser.json", UsedUser.class);
        registerPageHelper.checkUserIsAlreadyInAzure(usedUser.getEmail());

        if (APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(usedUser.getEmail())) {
            String tip = "This email is already in use. Please try signing in";
            registerPageHelper.assertEmailAlready(usedUser.getEmail(), tip);
        } else {
            assertThat("This email is already in register", false);
        }

    }

    @Story("Story: Test Register with mobile number")
    @Test(description = "Case-4: User cannot register with registered phone",priority = 2)
    @Description("Case Steps: " +
            "1. User clicks on Register on the OneApp - [1][2][3]\n " +
            "2. User is routed to the registration screen on OneApp\n " +
            "3. User clicks on Register Continue To Registration on the OneApp\n " +
            "4. User is routed to the registration screen on OneApp - [4]\n " +
            "5. User enters registered mobile number- [6][7][8][8.1][8.1.1][8.1.3][8.1.4] \n" +
            "Expected Result: System prompt: This email is already in use. Please try siging in")
    public void testCaseForEmail_04() throws Exception {
        driver.resetApp();
        startPageHelper.goRegister();
        continueRegisterPageHelper.goRegister();

        Utils.addAttachment("data", Utils.getAbsolutePath("users/useduser.json"));
        UsedUser usedUser = Utils.deserialize("users/useduser.json", UsedUser.class);
        registerPageHelper.checkUserIsAlreadyInAzure(usedUser.getPhone());
        if(APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithPhone(usedUser.getPhone())){
            String tip = "This mobile number is already in use. Please try signing in";
            registerPageHelper.assertMobileNumberAlready(usedUser.getPhone(), tip);
        }else {
            assertThat("This email is already in register",false);
        }

    }

    @Story("Story: Test Register with email")
    @Test(description = "Case-5: User cannot register with invalid email",priority = 3)
    @Description("Case Steps: " +
            "1. User clicks on Register on the OneApp - [1][2][3]\n " +
            "2. User is routed to the registration screen on OneApp\n " +
            "3. User clicks  on Register  Continue To Registration on the OneApp\n " +
            "4. User is routed to the registration screen on OneApp - [4]\n " +
            "5. User enters error email \n" +
            "Expected Result: System prompt: Please enter a valid email adress")
    public void testCaseForEmail_05() throws Exception {
        driver.resetApp();
        startPageHelper.goRegister();
        continueRegisterPageHelper.goRegister();

        String tip = "Please enter a valid email address";
        String email = "curefun001com";
        registerPageHelper.assertEmailError(email, tip);
    }

    @Story("Story: Test Register with mobile number")
    @Test(description = "Case-6: User cannot register with invalid phone number",priority = 4 )
    @Description("Case Steps: " +
            "1. User clicks on Register on the OneApp- [1][2][3]\n " +
            "2. User is routed to the registration screen on OneApp\n " +
            "3. User clicks  on Register  Continue To Registration on the OneApp\n " +
            "4. User is routed to the registration screen on OneApp - [4]\n " +
            "5. User enters error mobile number" +
            "Expected Result: System prompt: Please enter a valid mobile number")
    public void testCaseForEmail_06() throws Exception {
        driver.resetApp();
        startPageHelper.goRegister();
        continueRegisterPageHelper.goRegister();

        String tip = "Please enter a valid mobile number";
        String number = "1546775";

        registerPageHelper.assertMobileNumberError(number, tip);
    }

    @Story("Story: Test register with invalid email")
    @Test(description = "Case 1.1: Sign up - Activate via Email")
    @Description("Case Steps: \n" +
            "1. User click on register button \n" +
            "2. User click on continue register button \n" +
            "3. User enter all required info and click register button")
    public void testRegister1()  {
        try {
            driver.resetApp();
            startPageHelper.goRegister();
            continueRegisterPageHelper.goRegister();

            registerPageHelper.inputRegisterInfo("Nicholas", "Wong", "peterapiitgmail.com",
                    "4842989343", "123456Wkc");

            registerPageHelper.assertEmailError("peterapiitgmail.com", "Please enter a valid email address");
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Story("Story: Test register with invalid phone no")
    @Test(description = "Case 1.2: Sign up - Activate via Email")
    @Description("Case Steps: \n" +
            "1. User click on register button \n" +
            "2. User click on continue register button \n" +
            "3. User enter all required info and click register button")
    public void testRegister2()  {
        try {
            driver.resetApp();
            startPageHelper.goRegister();
            continueRegisterPageHelper.goRegister();

            registerPageHelper.inputRegisterInfo("Nicholas", "Wong", "peterapiit0@gmail.com",
                    "0000000000", "123456Wkc");

            registerPageHelper.assertMobileNumberError("0000000000", "Please enter a valid mobile number");
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Story("Test sign in functionality")
    @Test(description="Case 1.3: Sign in")
    @Description("Case Steps: " +
            "1. User click sign in button " +
            "2. User input username and password")
    public void testSignIn() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx_+279295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait homepageWait = new WebDriverWait(driver, 10);
            homepageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/toolbar_title")));
            MobileElement myGarage = driver.findElement(By.id("com.toyota.oneapp.stage:id/toolbar_title"));
            Assert.assertEquals(true, myGarage.isDisplayed());

        } catch (Exception e) {
            logger.error(e);
        }


    }

    @Story("Test sign in sign out functionality")
    @Test(description="Case : Sign out")
    @Description("Case Steps: \n " +
             "1. User sign in to Toyota One App by clicking sign in" +
             "2. User input email address" +
             "3. User input password")
    public void testSigninSignout() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx_+279295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait homepageWait = new WebDriverWait(driver, 10);
            homepageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/toolbar_title")));
            MobileElement account = driver.findElement(By.id("com.toyota.oneapp.stage:id/profilePage"));
            TimeUnit.MILLISECONDS.sleep(3000);
            account.click();

            WebDriverWait btnSignoutWait = new WebDriverWait(driver, 10);
            btnSignoutWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btn_signout")));
            MobileElement signoutBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btn_signout"));
            signoutBtn.click();

            WebDriverWait signoutPopUpWait = new WebDriverWait(driver, 10);
            signoutPopUpWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/title_template")));
            MobileElement signoutBtn1 = driver.findElement(By.id("android:id/button1"));
            signoutBtn1.click();

            WebDriverWait signoutWait1 = new WebDriverWait(driver, 10);
            signoutWait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/login_login_btn")));
            MobileElement signinBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/login_login_btn"));
            Assert.assertEquals(true, signinBtn.isDisplayed());


        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Story("Test sign in invalid email functionality")
    @Test(description="Case : Sign In Invalid Email")
    @Description("Case Steps: \n " +
            "1. User sign in to Toyota One App by clicking sign in" +
            "2. User input email address" +
            "3. Error prompt out")
    public void testSigninInvalidEmail() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();
            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit10@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Story("Test sign in functionality")
    @Test(description="Case 1.3: Sign in")
    @Description("Case Steps: " +
            "1. User click sign in button " +
            "2. User input username and password")
    public void testSignInInvalidPassword() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver, 10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx79295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();


        } catch (Exception e) {
            logger.error(e);
        }
    }


    @Story("Test sign in functionality")
    @Test(description="Case 1.3: Sign in")
    @Description("Case Steps: " +
            "1. User click sign in button " +
            "2. User input username and password")
    public void testSignInMultipleInvalidPassword() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver, 10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx79295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait passwordSignPageWait1 = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText1 = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx79295");

            WebDriverWait btnSignInEnableWait1 = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn1 = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait passwordSignPageWait2 = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText2 = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx79295");

            WebDriverWait btnSignInEnableWait2 = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn2 = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait passwordSignPageWait3 = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText3 = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx79295");

            WebDriverWait btnSignInEnableWait3 = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn3 = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

        } catch (Exception e) {
            logger.error(e);
        }
    }







}
