package autoframework.testcaes.cases;

import autoframework.mail.ReceiveMail;
import autoframework.testcaes.helper.APIHelper;
import autoframework.utils.Utils;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class ApiTest {

    //@Test
    public void test1() throws Exception {
        boolean s = APIHelper.checkUserExistInAzure("tao_toyota_test1@126.com");
        System.out.println(s);
    }

    //@Test
    public void test2() throws Exception {
        //boolean s = APIHelper.emailVerifiedFlagVerification("tao_toyota_test11@126.com");
        boolean s1 = APIHelper.emailVerifiedFlagVerification("tao_toyota_test21@126.com");
        System.out.println(s1);
    }

    //@Test
    public void test4() throws Exception {
        //boolean s = APIHelper.emailVerifiedFlagVerification("tao_toyota_test11@126.com");
        boolean s1 = APIHelper.emailVerifiedFlagVerification("curefun001@126.com");
        System.out.println(s1);
    }

    //@Test
    public void test5() throws Exception {
        //boolean s = APIHelper.emailVerifiedFlagVerification("tao_toyota_test11@126.com");
        boolean s1 = APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail("curefun00111@126.com");
        System.out.println(s1);

        s1 = APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail("curefun001@126.com");
        System.out.println(s1);
    }

    //@Test
    public void test6() throws Exception {
        //boolean s = APIHelper.emailVerifiedFlagVerification("tao_toyota_test11@126.com");
        boolean s1 = APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithPhone("1546775766");
        System.out.println(s1);

        s1 = APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithPhone("1582752181");
        System.out.println(s1);
    }

    //@Test
    public void test3() throws Exception {
        String path = Utils.getAbsolutePath("email/126.properties");
        ReceiveMail receiveMail = new ReceiveMail("Jack_tester005@126.com", "QZFNTIRKXETJNAKK", path);
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, -5); //把日期往后增加一天,整数  往后推,负数往前移动
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果

        String code = receiveMail.verificationCode("Your Toyota App Verification Code", date, "(?<=verification code: )\\d+");
        System.out.println("get verification code : " + code);
    }
}
