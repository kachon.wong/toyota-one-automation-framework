package autoframework.testcaes.cases;

import autoframework.testcaes.basetest.BaseTest;
import com.github.javafaker.Faker;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

@Epic("Epic: Account Setting Function Test")
@Feature("Feature: Account Settings Function Test")
public class AccountSettingsTest extends BaseTest {

    @Story("Story: Test Account Settings")
    @Test(description = "Case-2: Account Settings Test")
    @Description("Update Profile Info - Personal Details")
    public void updatePersonalDetails() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx_+279295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait homepageWait = new WebDriverWait(driver, 15);
            homepageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/toolbar_title")));
            MobileElement myAccount = driver.findElement(By.id("com.toyota.oneapp.stage:id/profilePage"));
            myAccount.click();

            WebDriverWait accountHomePageWait = new WebDriverWait(driver, 10);
            accountHomePageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/tv_greetings_name")));
            MobileElement personalInfoBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/tv_person_info"));
            personalInfoBtn.click();

            WebDriverWait personalDetailsWait = new WebDriverWait(driver, 10);
            personalDetailsWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/textView_personal_details")));
            MobileElement personalDetails = driver.findElement(By.id("com.toyota.oneapp.stage:id/textView_personal_details"));
            personalDetails.click();

            WebDriverWait personalDetailsPageWait = new WebDriverWait(driver, 10);
            personalDetailsPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/personal_details_header_txt")));
            MobileElement firstName = driver.findElement(By.id("com.toyota.oneapp.stage:id/et_first_name"));
            firstName.clear();
            Faker faker = new Faker();
            String data = faker.name().firstName();
            firstName.sendKeys(data);

            MobileElement saveBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/personal_detail_save_btn"));
            saveBtn.click();

            WebDriverWait saveNameWait = new WebDriverWait(driver,10);
            saveNameWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/tv_greetings_name")));
            MobileElement fullName = driver.findElement(By.id("com.toyota.oneapp.stage:id/tv_greetings_name"));
            String fullNameStr = fullName.getText();

            Assert.assertEquals(data + " Wong", fullNameStr);

        } catch (Exception e) {
            logger.error(e);
        }
    }


    @Story("Story: Test Account Settings")
    @Test(description = "Case-2.1: Account Settings Test")
    @Description("Update Profile Info - Invalid Email Address")
    public void updateInvalidEmail() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx_+279295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait homepageWait = new WebDriverWait(driver, 15);
            homepageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/toolbar_title")));
            MobileElement myAccount = driver.findElement(By.id("com.toyota.oneapp.stage:id/profilePage"));
            myAccount.click();

            WebDriverWait accountHomePageWait = new WebDriverWait(driver, 10);
            accountHomePageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/tv_greetings_name")));
            MobileElement personalInfoBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/tv_person_info"));
            personalInfoBtn.click();

            WebDriverWait personalDetailsWait = new WebDriverWait(driver, 10);
            personalDetailsWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/textView_personal_details")));
            MobileElement personalDetails = driver.findElement(By.id("com.toyota.oneapp.stage:id/textView_personal_details"));
            personalDetails.click();

            WebDriverWait personalDetailsPageWait = new WebDriverWait(driver,10);
            personalDetailsPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/personal_details_header_txt")));
            MobileElement emailAddress = driver.findElement(By.id("com.toyota.oneapp.stage:id/et_email_address"));
            emailAddress.click();

            WebDriverWait editEmailPageWait = new WebDriverWait(driver, 10);
            editEmailPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/et_edit_email_phone")));
            MobileElement editEmailAddress = driver.findElement(By.id("com.toyota.oneapp.stage:id/et_edit_email_phone"));
            editEmailAddress.clear();
            editEmailAddress.sendKeys("peterapiitgmail.com");

            WebDriverWait editErrorWait = new WebDriverWait(driver, 10);
            editErrorWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/textinput_error")));
            MobileElement tip = driver.findElement(By.id("com.toyota.oneapp.stage:id/textinput_error"));

            Assert.assertEquals(true, tip.isDisplayed());

            Thread.sleep(9000);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Story("Story: Test Account Settings")
    @Test(description = "Case-2.2: Account Settings Test")
    @Description("Update Profile Info - Invalid Phone No")
    public void updateInvalidPhoneNo(){
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx_+279295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait homepageWait = new WebDriverWait(driver, 15);
            homepageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/toolbar_title")));
            MobileElement myAccount = driver.findElement(By.id("com.toyota.oneapp.stage:id/profilePage"));
            myAccount.click();

            WebDriverWait accountHomePageWait = new WebDriverWait(driver, 10);
            accountHomePageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/tv_greetings_name")));
            MobileElement personalInfoBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/tv_person_info"));
            personalInfoBtn.click();

            WebDriverWait personalDetailsWait = new WebDriverWait(driver, 10);
            personalDetailsWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/textView_personal_details")));
            MobileElement personalDetails = driver.findElement(By.id("com.toyota.oneapp.stage:id/textView_personal_details"));
            personalDetails.click();

            WebDriverWait personalDetailsPageWait = new WebDriverWait(driver,10);
            personalDetailsPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/personal_details_header_txt")));
            MobileElement phoneNo = driver.findElement(By.id("com.toyota.oneapp.stage:id/et_phone_number"));
            phoneNo.click();

            WebDriverWait editPhoneNoPageWait = new WebDriverWait(driver, 10);
            editPhoneNoPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/et_edit_email_phone")));
            MobileElement phoneNoTxt = driver.findElement(By.id("com.toyota.oneapp.stage:id/et_edit_email_phone"));
            phoneNoTxt.clear();
            //phoneNoTxt.sendKeys("0000000000");   Bug here

            WebDriverWait editErrorWait = new WebDriverWait(driver, 10);
            editErrorWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/textinput_error")));
            MobileElement tip = driver.findElement(By.id("com.toyota.oneapp.stage:id/textinput_error"));

            Assert.assertEquals(true, tip.isDisplayed());


        } catch (Exception e) {
            logger.error(e);
        }
    }


    @Story("Story: Test Account Settings")
    @Test(description = "Case-2.3: Account Settings Test")
    @Description("Update Profile Info - Home Address")
    public void updateHomeAddress() {
        try {
            driver.resetApp();
            startPageHelper.goLogin();

            WebDriverWait waitUsernameAppear = new WebDriverWait(driver, 10);
            waitUsernameAppear.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etName")));
            MobileElement usernameInput = driver.findElement(By.id("com.toyota.oneapp.stage:id/etName"));
            usernameInput.sendKeys("peterapiit@gmail.com");

            WebDriverWait continueEnabled = new WebDriverWait(driver ,10);
            continueEnabled.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btContinue")));
            MobileElement continueBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btContinue"));
            continueBtn.click();

            WebDriverWait passwordSignPageWait = new WebDriverWait(driver, 10);
            passwordSignPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/etPassword")));
            MobileElement passwordText = driver.findElement(By.id("com.toyota.oneapp.stage:id/etPassword"));
            passwordText.sendKeys("Wlx_+279295");

            WebDriverWait btnSignInEnableWait = new WebDriverWait(driver, 10);
            btnSignInEnableWait.until(ExpectedConditions.elementToBeClickable(By.id("com.toyota.oneapp.stage:id/btSignIn")));
            MobileElement btnSignIn = driver.findElement(By.id("com.toyota.oneapp.stage:id/btSignIn"));
            btnSignIn.click();

            WebDriverWait homepageWait = new WebDriverWait(driver, 10);
            homepageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/toolbar_title")));
            MobileElement myAccount = driver.findElement(By.id("com.toyota.oneapp.stage:id/profilePage"));
            myAccount.click();

            WebDriverWait accountHomePageWait = new WebDriverWait(driver, 10);
            accountHomePageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/tv_greetings_name")));
            MobileElement personalInfoBtn = driver.findElement(By.id("com.toyota.oneapp.stage:id/tv_person_info"));
            personalInfoBtn.click();

            WebDriverWait personalDetailsWait = new WebDriverWait(driver, 10);
            personalDetailsWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/textView_billing_address")));
            MobileElement homeAddress = driver.findElement(By.id("com.toyota.oneapp.stage:id/textView_billing_address"));
            homeAddress.click();

            WebDriverWait homeAddressPageWait = new WebDriverWait(driver, 10);
            homeAddressPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/country")));
            Faker faker = new Faker();
            MobileElement streetAdress = driver.findElement(By.id("com.toyota.oneapp.stage:id/line_1"));
            streetAdress.clear();
            streetAdress.sendKeys(faker.address().streetAddress());
            MobileElement city = driver.findElement(By.id("com.toyota.oneapp.stage:id/city"));
            city.clear();
            city.sendKeys(faker.address().cityName());
            MobileElement state = driver.findElement(By.id("com.toyota.oneapp.stage:id/state"));
            state.click();
            TimeUnit.MILLISECONDS.sleep(2000);
            new TouchAction<>(driver).press(PointOption.point(294, 397)).release().perform();
            MobileElement zipCode = driver.findElement(By.id("com.toyota.oneapp.stage:id/zip_code"));
            zipCode.clear();
            zipCode.sendKeys("35007");
            MobileElement saveBtn =  driver.findElement(By.id("com.toyota.oneapp.stage:id/billing_address_btn"));
            saveBtn.click();

            WebDriverWait continueSubscribetionPageWait = new WebDriverWait(driver, 10);
            continueSubscribetionPageWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.toyota.oneapp.stage:id/subscription_continue_button")));
            MobileElement continueBtn1 = driver.findElement(By.id("com.toyota.oneapp.stage:id/subscription_continue_button"));
            continueBtn1.click();


        } catch (Exception e) {
            logger.error(e);
        }



    }
}
