package autoframework.testcaes.helper;

import autoframework.pom.pages.LoginForAccountPage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginForAccountPageHelper {
    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private LoginForAccountPage loginForAccountPage;
    public LoginForAccountPageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        loginForAccountPage = new LoginForAccountPage(this.driver);
    }

    @Step("input account ")
    public void inputAccount(String account) throws Exception {
        loginForAccountPage.inputAccount(account);
        loginForAccountPage.clickContinueButton();
    }

}
