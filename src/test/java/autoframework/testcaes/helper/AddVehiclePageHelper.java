package autoframework.testcaes.helper;

import autoframework.pom.pages.AddVehiclePage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddVehiclePageHelper {
    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private AddVehiclePage addVehiclePage;

    public AddVehiclePageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        addVehiclePage = new AddVehiclePage(this.driver);
    }

    @Step("check add your vehicle is loaded; - [17] [18] [19] [20] [21]")
    public void checkPageLoaded() throws Exception {
        addVehiclePage.checkAddVehicle();
    }

    @Step("check this user '{emailUser}' is register success - [OCPR]")
    public void checkUserRegisterSuccessInOCPRDataBase(String email) throws Exception {
        logger.info("check user is already in OCPRDataBase");
        assertThat("This email is not in OCPRDataBase: " + email, APIHelper.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(email), is(true));
    }
}
