package autoframework.testcaes.helper;

import autoframework.pom.pages.ContinueRegisterPage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ContinueRegisterPageHelper {

    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private ContinueRegisterPage continueRegisterPage;
    public ContinueRegisterPageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        continueRegisterPage = new ContinueRegisterPage(this.driver);
    }

    @Step("continue to registration.- [4] ")
    public void goRegister() {
        continueRegisterPage.clickContinueRegisterButton();
    }
}
