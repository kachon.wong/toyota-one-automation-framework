package autoframework.testcaes.helper;

import autoframework.pom.pages.LoginForPasswordPage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginForPasswordPageHelper {
    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private LoginForPasswordPage loginForPasswordPage;
    public LoginForPasswordPageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        loginForPasswordPage = new LoginForPasswordPage(this.driver);
    }

    @Step("input account . - [22]")
    public void inputPassword(String password) throws Exception {
        loginForPasswordPage.inputPassword(password);
        loginForPasswordPage.clickLoginInButton();
    }

}
