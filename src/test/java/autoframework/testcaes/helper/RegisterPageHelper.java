package autoframework.testcaes.helper;

import autoframework.pom.pages.RegisterPage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RegisterPageHelper {

    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private RegisterPage registerPage;

    public RegisterPageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        registerPage = new RegisterPage(this.driver);
    }

    @Step("SignUp UI is displayed to the user; - [5] [6] [7] [8] [8.2] [8.2.1] [8.2.2] ")
    public void inputRegisterInfo(String firstName, String lastName, String email, String mobileNumber, String password) throws Exception {
        registerPage.inputFirstName(firstName);
        registerPage.inputLastName(lastName);
        registerPage.inputEmail(email);
        registerPage.inputPassword(password);
        registerPage.inputMobileNumber(mobileNumber);
        registerPage.clickRegister();
    }

    @Step("Get the error info： This email is already in use. Please try signing in - [8.1][8.1.1][8.1.13][8.1.4]")
    public void assertEmailAlready(String email, String assertTip) throws Exception {
        registerPage.inputEmail(email);
        String tip = registerPage.getEmailIsAlready();
        logger.info("get tip is : " + tip);
        assertThat("Failed to get the error info： This email is already in use. Please try signing in", tip, is(assertTip));
    }

    @Step("Get the error info： Please enter a valid email address - [8.1]")
    public void assertEmailError(String email, String assertTip) throws Exception {
        registerPage.inputEmail(email);
        String tip = registerPage.getEmailIsError();
        logger.info("get tip is : " + tip);
        assertThat("Failed to get the error info： Please enter a valid email address", tip, is(assertTip));
    }

    @Step("Get the error info： This mobile number is already in use. Please try signing in - [8.1][8.1.1][8.1.13][8.1.4]")
    public void assertMobileNumberAlready(String mobileNumber, String assertTip) throws Exception {
        registerPage.inputMobileNumber(mobileNumber);
        String tip = registerPage.getMobileNumberIsAlready();
        logger.info("get tip is : " + tip);
        assertThat("Failed to get the error info： This mobile number is already in use. Please try signing in", tip, is(assertTip));
    }

    @Step("Get the error info： Please enter a valid mobile number - [8.1]")
    public void assertMobileNumberError(String mobileNumber, String assertTip) throws Exception {
        registerPage.inputMobileNumber(mobileNumber);
        String tip = registerPage.getMobileNumberError();
        logger.info("get tip is : " + tip);
        assertThat("Failed to get the error info： Please enter a valid mobile number", tip, is(assertTip));
    }

    @Step("check this user '{email}' is not already in Azure - [7][8]")
    public void checkUserIsNotAlreadyInAzure(String email) throws Exception {
        logger.info("check user is not already in Azure");
        assertThat("This email is not in Azure: " + email, APIHelper.checkUserExistInAzure(email), is(false));
    }


    @Step("check this user '{email}' is  already in Azure - [7][8]")
    public void checkUserIsAlreadyInAzure(String email) throws Exception {
        logger.info("check user is  already in Azure");
        assertThat("This email is  in Azure: " + email, APIHelper.checkUserExistInAzure(email), is(true));
    }
}
