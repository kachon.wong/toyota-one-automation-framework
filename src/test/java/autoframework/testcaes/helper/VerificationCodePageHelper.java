package autoframework.testcaes.helper;

import autoframework.pom.pages.VerificationCodePage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class VerificationCodePageHelper {

    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private VerificationCodePage verificationCodePage;

    public VerificationCodePageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        verificationCodePage = new VerificationCodePage(this.driver);
    }

    @Step("check Verification code page is load success")
    public void checkVerificationCodePage() throws Exception {
        verificationCodePage.checkVerificationCodePage();
    }

    @Step("On Successful user creation,IDM generates activation code; - [9] [10] [11] [12] [13] [14] [16] [17] [18] [19] [20] [21]")
    public void inputVerificationCode(String code) throws Exception {
        verificationCodePage.inputVerificationCode(code);
        verificationCodePage.clickVerifyAccount();
    }

    @Step("check user is not activate [14]")
    public void checkUserNotActivate(String email) throws Exception {
        logger.info("check user is not activate");
        assertThat("check user is not activate " + email, APIHelper.emailVerifiedFlagVerification(email), is(false));
    }

    @Step("check user is activate [14]")
    public void checkUserActivate(String email) throws Exception {
        logger.info("check user is  activate");
        assertThat("check user is activate " + email, APIHelper.emailVerifiedFlagVerification(email), is(true));
    }
}
