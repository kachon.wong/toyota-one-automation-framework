package autoframework.testcaes.helper;

import autoframework.pom.pages.StartPage;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StartPageHelper {
    private Logger logger = LogManager.getLogger(this.getClass());
    private AppiumDriver driver;
    private StartPage startPage;
    public StartPageHelper(AppiumDriver driver) throws Exception {
        this.driver = driver;
        startPage = new StartPage(this.driver);
    }

    @Step("user opens OneApp; - [1] [2] [3] ")
    public void goRegister() {
        startPage.clickRegisterButton();
    }

    @Step("user click Login In")
    public void goLogin() {
        startPage.clickLoginInButton();
    }
}
