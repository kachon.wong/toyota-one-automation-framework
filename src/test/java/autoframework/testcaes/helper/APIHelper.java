package autoframework.testcaes.helper;

import autoframework.api.base.HttpClient;
import autoframework.api.controller.AzureController;

public class APIHelper {

    public static boolean checkUserExistInAzure(String emailOrPhoneNumber) throws Exception {
        HttpClient httpClient = new HttpClient("https://login.microsoftonline.com");
        String result = AzureController.getToken(httpClient);

        HttpClient httpClient1 = new HttpClient("https://graph.windows.net/tmnab2cqa.onmicrosoft.com");
        boolean isExist = AzureController.checkUserExist(httpClient1, result, emailOrPhoneNumber);
        return isExist;
    }

    public static boolean emailVerifiedFlagVerification(String email) throws Exception {
        HttpClient httpClient = new HttpClient("https://openidm.stg.toyotadriverslogin.com");
        boolean isExist = AzureController.emailVerifiedFlagVerification(httpClient, email);
        return isExist;
    }

    public static boolean isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(String email) throws Exception {
        HttpClient httpClient = new HttpClient("https://sts.windows.net");
        String token = AzureController.getTokenForOCPRDataBaseValidation(httpClient);

        HttpClient httpClient1 = new HttpClient("https://api.stg.telematicsct.com");
        boolean isCreated = AzureController.isUserCreatedByUsingOCPRDataBaseValidationAlignWithEmail(httpClient1, token, email);
        return isCreated;
    }

    public static boolean isUserCreatedByUsingOCPRDataBaseValidationAlignWithPhone(String phone) throws Exception {
        HttpClient httpClient = new HttpClient("https://sts.windows.net");
        String token = AzureController.getTokenForOCPRDataBaseValidation(httpClient);

        HttpClient httpClient1 = new HttpClient("https://api.stg.telematicsct.com");
        boolean isCreated = AzureController.isUserCreatedByUsingOCPRDataBaseValidationAlignWithPhone(httpClient1, token, phone);
        return isCreated;
    }
}
