package autoframework.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmailUser {
    private String email;
    private String phone;
    private String passwordForEmail;
    private String firstName;
    private String lastName;
    private String passwordForAccount;
}
