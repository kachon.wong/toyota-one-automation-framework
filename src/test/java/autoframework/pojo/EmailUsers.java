package autoframework.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class EmailUsers {
    private List<EmailUser> users;
}
