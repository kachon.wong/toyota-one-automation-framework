package autoframework.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsedUser {
    private String email;
    private String phone;
}
