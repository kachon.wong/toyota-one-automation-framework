package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.StartPageObject;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StartPage extends BasePage {
    private Logger logger = LogManager.getLogger(StartPage.class);
    private StartPageObject startPageObject;
    public StartPage(AppiumDriver driver) {
        super(driver);
        startPageObject = new StartPageObject(driver, webUtils);
    }

    @Step("click register")
    public void clickRegisterButton() {
        logger.info("click register");
        webUtils.waitForElementVisible(startPageObject.register);
        webUtils.click(startPageObject.register);
    }

    @Step("click Login In")
    public void clickLoginInButton() {
        logger.info("click Login In");
        webUtils.waitForElementVisible(startPageObject.login);
        webUtils.click(startPageObject.login);
    }
}
