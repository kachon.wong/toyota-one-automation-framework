package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.VerificationCodePageObject;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VerificationCodePage extends BasePage {
    private Logger logger = LogManager.getLogger(VerificationCodePage.class);
    private VerificationCodePageObject verificationCodePageObject;
    public VerificationCodePage(AppiumDriver driver) {
        super(driver);
        verificationCodePageObject = new VerificationCodePageObject(driver, webUtils);
    }


    @Step("input Verification code")
    public void inputVerificationCode(String code) throws Exception {
        logger.info("input Verification code");
//        webUtils.changeContextToWebView("WEBVIEW_chrome");
//        webUtils.changeContextToLatestHandle();
//        webUtils.waitForElementVisible(verificationCodePageObject.verificationCode);
        webUtils.inputText(verificationCodePageObject.verificationCode,code);
    }
    @Step("check Verification code page is load success")
    public void checkVerificationCodePage() throws Exception {
        logger.info("check Verification code page is load success");
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(verificationCodePageObject.verificationCode);
    }


    @Step("click Verify Account")
    public void clickVerifyAccount() throws Exception {
        logger.info("click Verify Account");
        webUtils.click(verificationCodePageObject.verifyAccount);
    }
}
