package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.RegisterPageObject;
import autoframework.utils.Utils;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class RegisterPage extends BasePage {
    private Logger logger = LogManager.getLogger(RegisterPage.class);
    private RegisterPageObject registerPageObject;
    public RegisterPage(AppiumDriver driver) {
        super(driver);
        registerPageObject = new RegisterPageObject(driver, webUtils);
    }


    @Step("input First Name")
    public void inputFirstName(String firstName) throws Exception {
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(registerPageObject.firstName,90,500);
        logger.info("input  First Name");
        webUtils.inputText(registerPageObject.firstName,firstName);
    }

    @Step("input Last Name")
    public void inputLastName(String lastName) throws Exception {
        Utils.swipePage(driver,500,0.5,0.6,0.5,0.3);
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(registerPageObject.lastName,30,500);
        logger.info("input Last Name");
        webUtils.inputText(registerPageObject.lastName,lastName);
    }

    @Step("input email - [8.1]")
    public void inputEmail(String email) throws Exception {
        Utils.swipePage(driver,500,0.5,0.6,0.5,0.3);
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(registerPageObject.email,90,500);
        logger.info("input email");
        webUtils.inputText(registerPageObject.email,email);
    }

    @Step("input Mobile Number - [8.1]")
    public void inputMobileNumber(String mobileNumber) throws Exception {
        Utils.swipePage(driver,500,0.5,0.6,0.5,0.3);
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        logger.info("input Mobile Number");
        webUtils.waitForElementVisible(registerPageObject.mobileNumber);
        webUtils.inputText(registerPageObject.mobileNumber,mobileNumber);
    }

    @Step("输入 Password")
    public void inputPassword(String password) {
        logger.info("input Password");
        webUtils.inputText(registerPageObject.password,password);
    }

    @Step("click register,- [6][7][8][8.1][8.2]")
    public void clickRegister() throws Exception {
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(registerPageObject.register);
        logger.info("click Register");
        webUtils.click(registerPageObject.register);
    }

    @Step("get email is already tip - [8.1.1][8.1.13][8.1.4]")
    public String getEmailIsAlready() {
        logger.info("get email is already tip");
        webUtils.waitForElementVisible(registerPageObject.emailAlready);
        Utils.sleepBySecond(3);
        String tip = webUtils.getText(registerPageObject.emailAlready);
        return tip;
    }

    @Step("get tip :Please enter a valid email address")
    public String getEmailIsError() {
        logger.info("get tip : Please enter a valid email address");
        webUtils.waitForElementVisible(registerPageObject.emailAlready);
        Utils.sleepBySecond(3);
        String tip = webUtils.getText(registerPageObject.emailAlready);
        return tip;
    }

    @Step("get Mobile Number is already tip- [8.1.1][8.1.13][8.1.4]")
    public String getMobileNumberIsAlready() {
        logger.info("get mobile number is already tip");
        webUtils.waitForElementVisible(registerPageObject.mobileNumberAlready, 30, 500);
        Utils.sleepBySecond(3);
        String tip = webUtils.getText(registerPageObject.mobileNumberAlready);
        return tip;
    }

    @Step("get tip : Please enter a valid mobile number")
    public String getMobileNumberError() {
        logger.info("get tip : Please enter a valid mobile number");
        webUtils.waitForElementVisible(registerPageObject.mobileNumberAlready, 30, 500);
        Utils.sleepBySecond(3);
        String tip = webUtils.getText(registerPageObject.mobileNumberAlready);
        return tip;
    }
}
