package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.ContinueRegisterPageObject;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ContinueRegisterPage extends BasePage {
    private Logger logger = LogManager.getLogger(ContinueRegisterPage.class);
    private ContinueRegisterPageObject continueRegisterPageObject;
    public ContinueRegisterPage(AppiumDriver driver) {
        super(driver);
        continueRegisterPageObject = new ContinueRegisterPageObject(driver, webUtils);
    }


    @Step("click continue to registration")
    public void clickContinueRegisterButton() {
        logger.info("click continue to registration");
        webUtils.waitForElementVisible(continueRegisterPageObject.continueRegister);
        webUtils.click(continueRegisterPageObject.continueRegister);
    }
}
