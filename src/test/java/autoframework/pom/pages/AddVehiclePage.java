package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.AddVehiclePageObject;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AddVehiclePage extends BasePage {
    private Logger logger = LogManager.getLogger(AddVehiclePage.class);
    private AddVehiclePageObject addVehiclePageObject;
    public AddVehiclePage(AppiumDriver driver) {
        super(driver);
        addVehiclePageObject = new AddVehiclePageObject(driver, webUtils);
    }

    @Step("check add your vehicle page is loaded ")
    public void checkAddVehicle() throws Exception {
        logger.info("check add your vehiclecode page is loaded ");
        webUtils.changeContextToNativeApp();
        webUtils.waitForElementVisible(addVehiclePageObject.addVehicle,200,500);
    }
}
