package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.LoginForPasswordPageObject;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginForPasswordPage extends BasePage {
    private Logger logger = LogManager.getLogger(LoginForPasswordPage.class);
    private LoginForPasswordPageObject loginForPasswordPageObject;
    public LoginForPasswordPage(AppiumDriver driver) {
        super(driver);
        loginForPasswordPageObject = new LoginForPasswordPageObject(driver, webUtils);
    }

    @Step("input password")
    public void inputPassword(String password) throws Exception {
        logger.info("input password");
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(loginForPasswordPageObject.password);
        webUtils.inputText(loginForPasswordPageObject.password,password);

    }

    @Step("click Sign In button")
    public void clickLoginInButton() {
        logger.info("click Login In button");
        webUtils.click(loginForPasswordPageObject.singIn);
    }
}
