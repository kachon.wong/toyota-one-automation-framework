package autoframework.pom.pages;

import autoframework.common.basepage.BasePage;
import autoframework.pom.pageobjects.LoginForAccountPageObject;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginForAccountPage extends BasePage {
    private Logger logger = LogManager.getLogger(LoginForAccountPage.class);
    private LoginForAccountPageObject loginForAccountPageObject;
    public LoginForAccountPage(AppiumDriver driver) {
        super(driver);
        loginForAccountPageObject = new LoginForAccountPageObject(driver, webUtils);
    }

    @Step("input account : email or mobile number")
    public void inputAccount(String account) throws Exception {
        logger.info("input account : email or mobile number");
        webUtils.changeContextToWebView("WEBVIEW_chrome");
        webUtils.changeContextToLatestHandle();
        webUtils.waitForElementVisible(loginForAccountPageObject.account);
        webUtils.inputText(loginForAccountPageObject.account,account);

    }

    @Step("click Continue button")
    public void clickContinueButton() {
        logger.info("click Continue button");
        webUtils.click(loginForAccountPageObject.continueButton);
    }
}
