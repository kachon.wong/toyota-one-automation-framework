package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class LoginForAccountPageObject extends BasePageObject {
    public LoginForAccountPageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // acconut : Email or Mobile Number
    @FindBy(id = "tmnaUserNameInput")
    public MobileElement account;

    // Continue
    @FindBy(id = "loginButton_0")
    public MobileElement continueButton;
}
