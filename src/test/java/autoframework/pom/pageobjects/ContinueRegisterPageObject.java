package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class ContinueRegisterPageObject extends BasePageObject {
    public ContinueRegisterPageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // 继续 注册
    @FindBy(id = "com.toyota.oneapp.stage:id/continue_register_btn")
    public MobileElement continueRegister;
}
