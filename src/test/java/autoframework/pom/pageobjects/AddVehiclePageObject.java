package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class AddVehiclePageObject extends BasePageObject {
    public AddVehiclePageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // Add your Vehicle
    @FindBy(id = "com.toyota.oneapp.stage:id/tv_header")
    public AndroidElement addVehicle;
}
