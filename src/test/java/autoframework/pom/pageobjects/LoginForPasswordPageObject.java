package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class LoginForPasswordPageObject extends BasePageObject {
    public LoginForPasswordPageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // Password
    @FindBy(id = "idToken1")
    public MobileElement password;

    // Continue
    @FindBy(id = "loginButton_0")
    public MobileElement singIn;
}
