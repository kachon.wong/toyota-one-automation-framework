package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class VerificationCodePageObject extends BasePageObject {
    public VerificationCodePageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // VerificationCode
    @FindBy(id = "floatingLabelInput38")
    public WebElement verificationCode;

    //Verify Account button
    @FindBy(css = ".btn-lg")
    public WebElement verifyAccount;
}
