package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class RegisterPageObject extends BasePageObject {
    public RegisterPageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // First Namew
    @FindBy(id = "floatingLabelInput16")
    public WebElement firstName;

    //Last name
    @FindBy(id = "floatingLabelInput19")
    public WebElement lastName;

    //email
    @FindBy(id = "floatingLabelInput22")
    public WebElement email;

    //email tip
    @FindBy(css = ".tmna-fieldset-height .tmna-ul-policy-errors")
    public WebElement emailAlready;

    //Mobile Number
    @FindBy(id = "floatingLabelInput25")
    public WebElement mobileNumber;

    //mobile number tip
    @FindBy(css = ".tmna-fieldset-height .tmna-ul-policy-errors")
    public WebElement mobileNumberAlready;

    //Password
    @FindBy(id = "floatingLabelInput32")
    public WebElement password;

    //Register button
    @FindBy(css = ".align-items-center")
    public WebElement register;
}
