package autoframework.pom.pageobjects;

import autoframework.common.basepageobject.BasePageObject;
import autoframework.common.webutils.WebUtils;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class StartPageObject extends BasePageObject {
    public StartPageObject(RemoteWebDriver remoteWebDriver, WebUtils webUtils) {
        super(remoteWebDriver, webUtils);
    }
    // 注册
    @FindBy(id = "com.toyota.oneapp.stage:id/login_sign_up_btn")
    public MobileElement register;

    // 登录
    @FindBy(id = "com.toyota.oneapp.stage:id/login_login_btn")
    public MobileElement login;
}
